<br>
<h4>Listado de productos por Categoria</h4>
<?php  
//Recogemos el idCat de la barra de direcciones
if(isset($_GET['idCat'])){
	$idCat=$_GET['idCat'];

	//Comprobamos si idCat es un numero
	if(is_numeric($idCat)){

		//Nos pensamos la pregunta para sacar info de cat
		$sql="SELECT * FROM categorias WHERE idCat=$idCat";
		//ejecutar la pregunta (consulta)
		$consulta=mysqli_query($conexion, $sql);

		//MOSTRARE RESULTADOS .... SOLO si HAY 1 SOLA Categoria con ese id
		if(mysqli_num_rows($consulta)==1){

			//Extraemos el UNICO resultado
			$r=mysqli_fetch_array($consulta);
			?>
			<h4>
				<?php echo $r['nombreCat'];?>
				<small><?php echo $r['descripcionCat'];?></small>
			</h4>
			<hr>

			<?php  
			//Nos pensamos la pregunta para sacar info de producto de esa cat
			$sql="SELECT * FROM productos WHERE idCat=$idCat";
			//ejecutar la pregunta (consulta)
			$consulta=mysqli_query($conexion, $sql);
			//Voy a contar resultados, y si hay mas de 0, los muestro
			if(mysqli_num_rows($consulta)>0){
				//Extraemos LOS posibles resultados
				while($r=mysqli_fetch_array($consulta)){
					?>
					<article>
						<header>
							<h3><?php echo $r['nombreProd'];?></h3>
						</header>
						<section>
							<?php echo $r['descripcionProd'];?>
						</section>
					</article>
					<?php
				}
			}else{
				echo 'Lo siento... no hay productos en esta categoria';
			}

		}else{
			echo 'DEBERIA EXISTIR una SOLA CATEGORIA, y NO ES ASI....';
		}

	}else{
		echo 'EL codigo de idCat NO es NUMERICO';
	}

}else{
	echo 'Debo recibir un numero de idCAt';
}	
?>

