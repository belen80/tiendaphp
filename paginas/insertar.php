<br>
<h4>Alta de producto</h4>

<form role="form" method="post" action="index.php?p=insercion.php">
  
  <div class="form-group">
    <label for="nombreProd">Nombre de producto</label>
    <input type="text" class="form-control" id="nombreProd" name="nombreProd" placeholder="Introduce el nombre de producto">
  </div>

  <div class="form-group">
    <label for="precioProd">Precio de producto</label>
    <input type="text" class="form-control" id="precioProd" name="precioProd" placeholder="Introduce el precio del producto">
  </div>

  <div class="form-group">
    <label for="unidadesProd">Unidades del producto</label>
    <input type="text" class="form-control" id="unidadesProd" name="unidadesProd" placeholder="Introduce las unidades del producto">
  </div>

  <div class="form-group">
    <label for="descripcionProd">Descripcion del producto</label>
    <textarea class="form-control" rows="3" id="descripcionProd" name="descripcionProd"></textarea>
  </div>

  <div class="form-group">
    <label for="idCat">Categoria del producto</label>
    <select class="form-control" id="idCat" name="idCat">
      <?php
      $sql="SELECT * FROM categorias ORDER BY nombreCat ASC";
      $consulta=mysqli_query($conexion, $sql);
      while($r=mysqli_fetch_array($consulta)){
        ?>
        <option value="<?php echo $r['idCat'];?>">
          <?php echo $r['nombreCat'];?>
        </option>
        <?php
      }
      ?>
    </select>
  </div>

  <div class="form-group">
    <input type="submit" class="form-control" name="insertar" value="Alta de producto">
  </div>


</form>
